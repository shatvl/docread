<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use AppBundle\Repository\DocumentRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template("")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var DocumentRepository $documentRepo */
        $documentRepo = $em->getRepository(Document::class);
        $documents = $documentRepo->findAll();

        return $this->render('::index.html.twig', ['docs' => $documents]);
    }
}
