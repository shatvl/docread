<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 02.06.2017
 * Time: 14:39
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Attachment;
use AppBundle\Entity\Document;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DocumentController extends BaseController
{
    /**
     * @Route("/create-document", name="createDocument")
     * @param Request $request
     * @return Response
     */
    public function createDocumentAction(Request $request)
    {
        if ($request->get('edit')) {
            $documentRepo = $this->getDoctrine()->getRepository(Document::class);
            $document = $documentRepo->find($request->get('id'));
            return $this->render(':document:document.html.twig', ['doc' => $document, 'attch' => $document->getAttachments()->toArray()]);
        }

        return $this->render(':document:document.html.twig');
    }

    /**
     * @param Request $request
     *
     * @Route("/document")
     * @Method("POST")
     * @return Response
     */
    public function postDocumentAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()){
            return $this->getResponse([], self::STATUS_ERROR);
        }

        $document = null;
        if (!!($docId = $request->request->get('id'))) {
            $document = $this->getDoctrine()->getRepository(Document::class)->find($docId);
        }
        if (!$document) {
            $document = new Document();
        }

        $document
            ->setName($request->request->get('title'))
            ->setDescription($request->request->get('text'));

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $em->persist($document);
        $em->flush();

        return $this->getResponse($document->getId());
    }

    /**
     * @param Request $request
     *
     * @Route("/upload/attach")
     * @Method("POST")
     * @return Response
     */
    public function postUploadAttachAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()){
            return $this->getResponse([], self::STATUS_ERROR);
        }
        $em = $this->getDoctrine()->getManager();

        $document = null;
        if (!!($docId = $request->request->get('doc-id'))) {
            /** @var Document $document */
            $document = $this->getDoctrine()->getRepository(Document::class)->find($docId);
        }

        /** @var UploadedFile $file */
        foreach ($request->files as $file) {
            $attachment = new Attachment();
            $attachment
                ->setName($file->getClientOriginalName())
                ->setSize($file->getClientSize())
                ->setPosition(-1)
            ;

            $em->persist($attachment);
            if (!$document) {
                $document = new Document();
                $document
                    ->setName('Untitled_' . time())
                    ->setDescription('empty')
                ;
            }

            $document->setAttachments($attachment);

            $em->persist($document);
            $name = md5($file->getClientOriginalName() . time()) . '.' . $file->guessExtension();
            $file->move($this->getUploadDir(), $name);
        }

        $em->flush();
        return $this->getResponse($document->getId());
    }

    /**
     * @Route("/delete-document", name="deleteDocument")
     * @Method("DELETE")
     */
    public function deleteDocumentAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()){
            return $this->getResponse([], self::STATUS_ERROR);
        }

        $docID = $request->request->get('id');
        $doc = $this->getDoctrine()->getRepository(Document::class)->find($docID);

        if (!$doc) {
            $this->getResponse([], self::STATUS_ERROR);
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($doc);
            $em->flush();
        }

        return $this->getResponse([]);
    }

    /**
     * @Route("/delete-attachment")
     * @Method("DELETE")
     */
    public function deleteAttachment(Request $request)
    {
        if (!$request->isXmlHttpRequest()){
            return $this->getResponse([], self::STATUS_ERROR);
        }

        $doc = $this->getDoctrine()->getRepository(Document::class)->find($request->request->get('docId'));
        $attachment = $this->getDoctrine()->getRepository(Attachment::class)->find($request->request->get('id'));

        if (!$doc || !$attachment) {
            $this->getResponse([], self::STATUS_ERROR);
        } else {
            $em = $this->getDoctrine()->getManager();
            $doc->removeAttachments($attachment);

            $em->persist($doc);
            $em->flush();
        }

        return $this->getResponse([]);
    }

    /**
     * @return string
     */
    private function getUploadDir() {
        return __DIR__ . '/../../../web/attachmets';
    }
}