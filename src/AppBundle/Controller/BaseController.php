<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 05.06.2017
 * Time: 14:16
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    const STATUS_SUCCESS = 'true';
    const STATUS_ERROR = 'false';

    protected function getResponse($data, $success = self::STATUS_SUCCESS, $code = \Symfony\Component\HttpFoundation\Response::HTTP_OK)
    {
        $result = array(
            'data' => $data,
            'success' => $success,
            'code' => $code
        );

        return new Response(json_encode($result));
    }
}