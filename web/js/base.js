function deleteDoc(id) {
    $.ajax({
        method: 'DELETE',
        url: '/delete-document',
        data: {id: id},
        success: function(data) {
            var data = JSON.parse(data);
            if (data.success === "true") {
                $.growl.notice({ title: 'Success!', message: "Document has been deleted." });
                window.location.href = '/';
            } else {
                console.log('error');
            }
        }
    });
};

