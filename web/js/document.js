$(function () {
    $('#create-doc-form').on("submit", function(event) {
        event.preventDefault();
        var data = $('#create-doc-form').serialize();
        $.ajax({
            url: '/document',
            method: 'post',
            data: data,
            success: function(data) {
                var data = JSON.parse(data);
                if (data.success === "true") {
                    $.growl.notice({ title: 'Success!', message: "Document has been created." });
                    $('#doc-id').val(data.data);
                    window.location.href = '/';
                } else {
                    console.log('error'); //change to notify
                }
            }
        });
    });

    initDropZone();

    $('#upload-attach').on("submit", function(event) {
        event.preventDefault();
        var data = new FormData();
        data.append('file', $('#att-file')[0].files[0]);
        data.append('doc-id', $('#doc-id')[0].value);
        $.ajax({
            url: '/upload/attach',
            method: 'post',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var data = JSON.parse(data);
                if (data.success === "true") {
                    $('#doc-id').val(data.data);
                    $('#myModal').modal('hide');
                    $.growl.notice({ title: 'Success!', message: "Attachment has been uploaded." });
                    window.location.href = '/';
                } else {
                    console.log('error');
                }
            }
        })
    });

    $('#myModal').on('hidden.bs.modal',function(){
        $('#preupload-text').html('+ Upload File');
    });

    $("tbody").sortable({
        items: "> tr:not(:first)",
        appendTo: "parent",
        helper: "clone",
        update: function (e,item) {
            //update attachments positions
        }
    }).disableSelection();
});

function deleteAtt(id) {
    $.ajax({
        method: 'DELETE',
        url: '/delete-attachment',
        data: {id: id, docId: $('#doc-id')[0].value},
        success: function(data) {
            var data = JSON.parse(data);
            if (data.success === "true") {
                $.growl.notice({ title: 'Success!', message: "Attachment has been deleted." });
                window.location.href = '/';
            } else {
                console.log('error');
            }
        }
    });
};


function initDropZone() {
    $('#dropzone').dropzone({ url: "/file/post" });

    $('#dropzone').on('dragover', function() {
        $(this).addClass('hover');
    });

    $('#dropzone').on('dragleave', function() {
        $(this).removeClass('hover');
    });

    $('#dropzone input').on('change', function(e) {
        var file = this.files[0];

        $('#dropzone').removeClass('hover');

        if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
            return alert('File type not allowed.');
        }

        $('#dropzone').addClass('dropped');
        $('#dropzone img').remove();

        if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
            var reader = new FileReader(file);

            reader.readAsDataURL(file);

            reader.onload = function(e) {
                var data = e.target.result,
                    $img = $('<img />').attr('src', data).fadeIn();

                $('#dropzone div').html($img);
            };
        } else {
            var ext = file.name.split('.').pop();

            $('#dropzone div').html(ext);
        }
    });
};